################################################
####### HEAD  test                            ######
################################################
27.11.2016 ->
by Maarten

- fixed SELinux context of VOBOX proxy renewal log directory

- adjusted gsisshd configuration for CentOS/EL 7 and SELinux

- adjusted spec file and Makefile to a more standard build environment

- new repository: http://svnweb.cern.ch/world/wsvn/curios/glite-yaim-clients

01.03.2013->
by Cristina

config/functions/config_glite_ui, config_emi_nagios, config_glite_init, config_emi_ui_tar
- removed /opt/lcg/bin - fix for GGUS #91216

17.01.2013 ->
by Maarten
config_gsissh and config_vobox
- updated for a "WLCG VOBOX" based on the EMI UI

03.04.2012->
by Cristina

config/functions/config_emi_ui_tar
- added new env-var EMI_UI_CONF for UI_tar

15.03.2012->
by Cristina

config/functions/config_glite_saga
- corrected according to branch_glite

15.03.2012->
by Cristina

changes in various functions and additions so that TARBALLS work
- added defaults/emi-wn_tar

12.03.2012->
by Cristina

added SAGA configuration & new vars for tarballs
-removed config_rgma_client, config_globus_devel, config_gsissh

29/01/2012 ->
by Cristina

config/functions/config_fts_client
- fix for bug #85670

################################################
####### TAG glite-yaim-clients_R_5_0_0_1 #######
################################################

17/04/2011 ->
by Cristina

config/functions/config_wn, config/defaults/glite-wn.post
- adapt to EMI new paths

05/04/2011 ->
by Cristina

config/functions/config_glite_ui
- readded creation of glite_wmsui_cmd_var.conf

05/04/2011 ->
by Cristina

node-info.d/glite-ui
- temporary removed config_gsissh


01/04/2011 ->
by Cristina

node-info.d/glite-ui, functions/*, defaults/glite-ui.post
- changes for EMI

config/functions/config_glite_saga
- moved from glite-yaim-core

01/04/2011 ->
by Cristina

node-info.d/glite-[ui|wn]
- removed config_rgma_client - function not needed

config/functions/config_fix_edg-fetch-crl-cron
- removed 3.0 function, no more needed



11/3/2011 ->
by David Smith

- remove unused file install_certs_userland

config/functions/config_globus_devel
- Fix for bug #55570: TAR UI SL5 configuration error.

config/functions/config_certs_userland
- Fix for bug #77901: UI_TAR and WN_TAR do not create CRL cron job!
- Fix for bug #78292: CA_REPOSITORY must currently be formatted as an APT repository link

config/functions/config_glite_ui
- Fix for bug #76237: UI tarball install doesn't setup openssl correctly

################################################
####### TAG glite-yaim-clients_R_4_0_11_2 ######
################################################

21/09/2010 -> 
by Andrew Elwell

* updated glue2 provider as per comment #3 in 
bug #62213

################################################
####### TAG glite-yaim-clients_R_4_0_11_1 ######
################################################

10/09/2010 ->

by Andrew Elwell

* removed all _30 files and rook out the RB config
(fixes for savannah #71831 / GGUS #61509)

* updated spec / licence to ASL 2.0

################################################
####### TAG glite-yaim-clients_R_4_0_10_1 ######
################################################

17/08/2010 ->

by Maria Alandes

config/node-info.d/glite-ui, glite-wn, glite-ui_tar and glite-wn_tar
- Fix for bug #71532: SAGA configuration function has been added

30/03/2010 ->

by David Smith

config/functions/config_glite_ui
- Fix for bug #65096: Spurious warning about missing OUTPUT_STORAGE file

config/functions/config_workload_manager_client
- Fix for bug #65102: Setting up the RB client should be optional on the UI

###############################################
####### TAG glite-yaim-clients_R_4_0_9_2 ######
###############################################

05/08/2009 ->

by Maria Alandes

config/functions/config_amga_client
- Fixed after certification. Wrong -d argument.

###############################################
####### TAG glite-yaim-clients_R_4_0_9_1 ######
###############################################

14/07/2009 ->

by Maria Alandes

config/functions/config_amga_client and config_glite_ui
- Fix for bug #53047: Check whether paths exist before adding them.


07/07/01 ->

by Maria Alandes

config/functions/config_wn_info
- Fix for bug #51372: mechanism for tarball installations.
- Fixed after testing: do the grep on ${WN_LIST}

config/functions/config_ui_tar
- Fix for bug #52825: GLITE_EXTERNAL_ROOT/usr/lib64 is now added to LD_LIBRARY_PATH

node-info.d
.	glite-ui
.	glite-ui_tar
.	glite-vobox
.	glite-wn
.	glite-wn_tar
- Fix for bug #52820: config_myproxy_libs has been added to the function lists.
- Remove config_myproxy_libs until issue with Myproxy clients is confirmed

config/functions/config_ui_tar and config_wn_tar
- Fix for bug #51263: require GLITE_EXTERNAL_ROOT
- Fixed after testing: removed duplicated functions

###############################################
####### TAG glite-yaim-clients_R_4_0_8_7 ######
###############################################

06/02/2009 ->

by Maria Alandes

config/functions/config_glite_ui
- Fix for bug #51148: -d is used instead of -f

###############################################
####### TAG glite-yaim-clients_R_4_0_8_6 ######
###############################################

14/05/2009 ->

by Maria Alandes

config/functions/config_glite_ui
- Fixing the most strange and stupid error I've ever seen

###############################################
####### TAG glite-yaim-clients_R_4_0_8_5 ######
###############################################

14/05/2009 ->

by Maria Alandes

config/defaults/glite-ui.post
- Allow to define glite-ui.post variables in site-info.def

###############################################
####### TAG glite-yaim-clients_R_4_0_8_4 ######
###############################################

14/05/2009 ->

by Maria Alandes

config/functions/config_glite_ui
- Fix while testing the SL5 UI

###############################################
####### TAG glite-yaim-clients_R_4_0_8_3 ######
###############################################

30/04/2009 ->

by Maria Alandes

config/functions/config_ui_tar and config_wn_tar
- Fix for bug #49810: perl 5.8.8 libs are added to PERL5LIB in TAR UI and TAR WN.

###############################################
####### TAG glite-yaim-clients_R_4_0_8_2 ######
###############################################

23/04/2009 ->

by Maria Alandes

config/functions/config_sw_dir
Improve config_sw_dir:
- require GROUPS_CONF
- print better warning messages


config/functions/config_glite_ui
- Fix while testing: yaimlog instead of yaim.

config/functions/config_glite_ui
- Fix for bug #49436: python2.3 or python2.4 paths are now added in the PYTHONPATH if the exist.
- Fix for bug #49271: VDT script to setup openssl is now executed.

###############################################
####### TAG glite-yaim-clients_R_4_0_8_1 ######
###############################################


08/04/2009 ->

by Maria Alandes

config/functions/config_info_service_vobox
- Fixed wrong aux variable.

07/04/2009 ->

by Maria Alandes

config/functions/config_wn
config/services
.	glite-wn
.	glite-wn_tar
- Fix for bug #45565: GLITE_LOCAL_CUSTOMIZATION_DIR is now added to grid-env.(c)sh if it exists.

config/functions/config_glite_ui
config/defaults
.	glite-ui.post
.	glite-ui.pre
.	glite-ui_tar.post
.	glite-ui_tar.pre
- Fix for bug #46146: GLITE_SD_PLUGIN and GLITE_SD_SERVICES_XML have now default values 
and are part of the grid-env.(c)sh.

config/defaults/glite-wn.post
- Fix description.

config/functions/config_info_service_vobox
- Fix for bug #47668: Supported VOs are now published by the VOBOX.

config/functions/config_glite_ui
- Fix for bug #48767: WMProxyEndpoints = {$WMPROXY_END_POINTS} has been added in glite_wmsui.conf

config/functions/config_glite_ui and config_wn
- Fix for bug #48044: classads won't be under externals in the tarballs. Put INSTALL_ROOT instead.

03/04/2009 ->

by Maria Alandes

config/functions/config_glite_ui and config_wn
- Fix for bug #48044: classads path is added to the LD_LIBRARY_PATH (in the tarballs this is expected to be installed in externals).

###############################################
####### TAG glite-yaim-clients_R_4_0_7_3 ######
###############################################

23/04/2009 ->

by Maria Alandes

config/node-info.d/glite-vobox
- Fix for bad fix: removing config_gip_vo_tag instead of config_gip_vobox.


###############################################
####### TAG glite-yaim-clients_R_4_0_7_2 ######
###############################################

20/02/2009 ->

by Maria Alandes

config/node-info.d/glite-vobox
- Fix in bug #47208: Remove config_gip_vo_tag from VOBOX

###############################################
####### TAG glite-yaim-clients_R_4_0_7_1 ######
###############################################


11/02/2009 ->

by Maria Alandes

config/node-info.d/glite-ui_tar and glite-wn_tar
- Fix for bug #42943: config_vomsdir is now included in TAR UI and TAR WN.

config/functions/config_glite_ui and config_wn
- Fix for bug #46848: PYTHONPATH definition is removed from clients configuration when it adds /opt/lcg/lib(/python) 
paths since this is done by config_lcgenv in yaim core.
- Fix for bug #46848: PERLLIB5 definition is removed from clients configuration since this is done by config_lcgenv in 
yaim core.


###############################################
####### TAG glite-yaim-clients_R_4_0_6_1 ######
###############################################

17/12/2008 ->

by Maria Alandes

config/defaults/glite-wn.post and glite-wn_tar.post
- New variable WN_INFO_CONFIG_FILE so that the glite-wn-info config file location can be chosen. 
Also config_wn_info is modified to use the new variable.

node-info.d/glite-wn and glite-wn_tar
- Fix for bug #45442: config_wn_info is now added to WN and TAR WN.

config/functions/config_vo_tag_dir and config_wn_info
- Improvements after testing
- I made an error. VO tag directories creation should go into the cluster module. config_vo_tag_dir deleted.
 

16/12/2008 ->

by Maria Alandes

config/functions/config_vo_tag_dir and config_wn_info
- New functions needed for cluster configuration that will be called by the WN.


27/11/2008 ->

by Maria Alandes

All glexec_wn related functions and config files:
- Fix for bug #44650: Remove glexec_wn functions and add them to glite-yaim-glexec-wn

###############################################
####### TAG glite-yaim-clients_R_4_0_5_5 ######
###############################################

25/11/2008 ->

by Maria Alandes

config/functions/config_glexec_wn_lcaslcmaps
Update: After Oscar comment #21 in patch #2635:
- Add allow-limited-proxy for log-only/no SCAS/syslog
- Remove good plugin and add verify_proxy for log-only/SCAS/syslog


###############################################
####### TAG glite-yaim-clients_R_4_0_5_4 ######
###############################################

20/11/2008 ->

by Maria Alandes

config/functions/config_glite_ui
- Fixed a STUPID typo

###############################################
####### TAG glite-yaim-clients_R_4_0_5_3 ######
###############################################

18/11/2008 ->

by Maria Alandes

config/functions/config_glexec_wn
- Fix: wrong creation of users white list. Now it's fixed.

config/functions/config_glexec_wn_lcaslcmaps
- Add: support for SCAS in log-only mode

config/functions/config_glexec_wn_log
- Remove: comment about glexec log-only bug

###############################################
####### TAG glite-yaim-clients_R_4_0_5_2 ######
###############################################

14/11/2008 ->

by Maria Alandes

config/defaults/glite-glexec_wn.post
- Fixed yaim core bug #43699 also affecting post files in clients

###############################################
####### TAG glite-yaim-clients_R_4_0_5_1 ######
###############################################

12/11/2008 ->

by Maria Alandes

config/functions/config_glite_ui
- Add missing scenario for service discovery configuration

config/functions/config_wn
- Fix for bug #43881: 64bit OS with 32bit middleware is now properly configured.
- Put 32bit libraries before 64bit libraries

11/11/2008 ->

by Maria Alandes

config/functions/config_glexec_wn
- /usr/lib should be included in the ld.so.conf for the TAR WN
- glexec.conf file location is fixed for the time being in /opt/glite/etc/glexec.conf
- Remove: preserve_env_variables for X509 certdir and vomsdir for the TAR WN
- Add: create the glexec.conf dir and file if it doesn't exist

config/functions/config_glexec_wn_lcaslcmaps
- ban_users.db should contain full path

10/11/2008 ->

by Maria Alandes

config/functions/config_glite_ui 
- Fix for bug #43603: Introduce new variable: UIWMS_SERVICE_DISCOVERY.

config/functions/config_glexec_wn_lcaslcmaps
- Add: https:// in the scas plugin for the lcmaps configuration.

config/functions/config_glexec_wn_log
- Fix warning messages

30/10/2008 ->

by Maria Alandes

config/defaults/
glite-glexec_wn.post 
glite-glexec_wn.pre 
- Move lcas/lcmaps log file definition to post since it depends on the dir defined in pre.

config/functions/config_glexec_wn_log 
- Add warning message in case log file is not writable in logging only mode

config/functions/config_glexec_wn_lcaslcmaps 
- Add new lcmaps configuration suggested by Oscar

29/10/2008 ->

by Maria Alandes

config/node-info.d/glite-glexec_wn 
- Add new functions in function list

config/functions/
config_glexec_wn 
config_glexec_wn_lcaslcmaps 
config_glexec_wn_log 
config_glexec_wn_users 
- Split config_glexec_wn in several functions
- Update require variable list
- Added missing 'done'

28/10/08 ->

by Maria Alandes

config/defaults/glite-glexec_wn.pre 
config/functions/config_glexec_wn 
- Add:
  - New variable to define the pilot job special users flag
  - More generic code to retrieve the glexec user white list for ordinary pool accounts and special accounts
  - Last lcmaps configuration provided by Oscar. Probably this will change.

17/10/2008 ->

by Maria Alandes

config/functions/config_glexec_wn 
- Add: LCAS configuration is different if SCAS is used or not.

16/10/2008 ->

by Maria Alandes

config/functions/config_glexec_wn 
- Add: checking of GLEXEC_WN_SCAS_ENABLED value

config/functions/config_glite_ui 
- Fix for bug #41494: warning message is now displayed.

config/functions/config_glexec_wn 
- Fix for bug #41237: new parameter added for verify-proxy

functions/config_glexec_wn and config/services/glite-glexec_wn 
- Add: support to work with and without SCAS.

config/functions/config_sw_dir 
- Fix for bug #40654: VOS is now included

config/node-info.d/glite-wn and glite-wn_tar 
- Fix for bug #40732: config_vomses is now part of the WNs

config/functions/config_gip_vobox 
- Fix for bug #40575: cleaning tasks in the old gip vobox function.

config/functions/config_info_service_vobox 
- Fix for bug #40575: new config_info_service_vobox

config/node-info.d/glite-vobox 
- Fix for bug #40563: config_gip_service_release is now in VOBOX
- Fix for bug #40575: add new config_info_service_vobox function in the function list.

config/defaults/glite-vobox.pre 
- Fix for bug #40199: removed duplicated variables

15/10/2008 ->

by Maria Alandes 

Makefile and glite-yaim-clients.spec: 
- Added services directory

14/10/2008 ->

by Maria Alandes

/config/defaults/glite-glexec_wn.pre 
/config/functions/config_glexec_wn 
/config/services/glite-glexec_wn 
Add:
- Renaming all variables to GLEXEC_WN_*
- Improvement with SCAS configuration instructions provided in http://www.nikhef.nl/grid/lcaslcmaps/scas/scas-release-notes_08-oct-2008.txt
- Created defaults/glite-glexec_wn.post since I was defining variables relying on INSTALL_ROOT

09/09/2008 ->

by Ricardo Mendes

config/functions/config_wn_tar 
- Fix for bug #40944: appended 64bit libraries path
- Removed ${INSTALL_ROOT}

25/08/2008 ->

by Maria Alandes

config/functions/config_gip_vobox and config_vobox 
- Add: return 0


01/09/2008 ->

by Maria Alandes

config/functions/config_glexec_wn
Update:
- Make lcmaps change user id for setuid mode
- Make glexec.conf readable in log-only mode
- Make log file writable in log-only mode

22/08/2008 ->

by Maria Alandes

config/defaults/glite-glexec_wn.pre 
- Add: LCAS and LCMAPS configuration files are configurable. Here are the defaults.
- Add: LCAS/LCMAPS log file

config/functions/config_glexec_wn 
- Add: check GLEXEC_OPMODE is correct
- Remove: now these variables are defined in /defaults


21/08/2008 ->

by Maria Alandes

config/functions/config_glexec_wn
- Update: ldconfig must be configured for setuid and logging only mode as well
- glexec bin ownerships are changed after glexec user and group are created. Otherwise there was an error.
- Fixed typo
- Fixed wrong error messages
- Removed a line by mistake
- Added ownership and permissions in glexec.conf and glexec bin according to John wiki
- Added user while list for logging only mode
- Improved internal variables

20/08/2008 ->

by Maria Alandes

config/functions/config_glexec_wn
- Added comments
- Add: support in the TAR WN for preservation of X509_CERT_DIR and X509_VOMS_DIR when they are not in standard locations.
- fix error with ld.so.conf
- add -s in case logging only mode configuration

19/08/2008 ->

by Maria Alandes

config/functions/config_glexec_wn
- Update: add ldconfig configuration for the TAR WN
- Update: hardcode INSTALL_ROOT=/opt because of current glexec configuration

18/08/2008 ->

by Maria Alandes

config/functions/config_glexec_wn
- Fixed missing 'then'
- Put back creation of glexec user.
- Change the function list order.
- Add the creation of edguser group.

15/08/2008 ->

by Maria Alandes Pradillo

config/services/glite-glexec_wn 
- New services configuration file for glexec wn

config/defaults/glite-glexec_wn.pre 
- New variable files for glexec_wn

config/node-info.d/glite-glexec_wn 
- Improve config target
- New node type

config/functions/config_glexec_wn 
- Change order
- Update: Add checking of glexec bin existing. Error otherwise.
- Update: remove the TAR WN part since this can only be called by root.
- Fixed typo
Update: 
- Introduce operational modes 
- use variables instead of hardcoded paths in the lcmaps configuration
- improve the 'requires' list of variables


13/08/2008 ->

by Maria Alandes Pradillo

config/functions/config_glite_ui 
- Fix for bug #39939: Remove NSAddresses from glite_wmsui.conf

config/functions/config_vobox 
- Remove: unlocking sgm special account since accounts are already unlocked.

config/functions/config_glexec_wn
- Update: error if no pilot user accounts are present in site-info.def
Improve: 
- config_glexec_wn doesn't create glexec group and user because this is not needed
- config_glexec_wn has been improved to work in TAR WN, although this has to be tested
- Update: improvement to cope with TAR WN

12/08/2008 ->

by Maria Alandes Pradillo

config/functions/config_glexec
- Update: add error message if user configuration disabled and glexec user and group not defined

spec file
- Add: %config

config/functions/
config_workload_manager_client
config_gsissh 
- Add: 'return 0'

config/node-info.d/glite-ui
- Remove: config_rfio from 3.1 UI. It's not needed.

07/08/2008 ->

by Maria Alandes Pradillo

config/node-info.d/glite-ui
- Fix for bug #38236: changed the order for the globus configuration in the UI to avoid strange build directory

config/functions/config_workload_manager_client 
- Fix for bug #39112: _check function was missing

Makefile and spec file
- Fix for bug #36693: YAIM version for yaim clients

config/functions/config_glexec_wn

Fix for bug #38373: 
- VOMS support for LCMAPS is now added. 
- Logging only mode included as an option with the variable GLEXEC_LOGGING_ONLY
- user_white_list restricted to pilot jobs


###############################################
####### TAG glite-yaim-clients_R_4_0_4_2 ######
###############################################

26/06/2008 ->

by Maria Alandes Pradillo

config/functions/config_glite_ui:
- Update: remove wrong ';' in the glite-wms.conf file

###############################################
####### TAG glite-yaim-clients_R_4_0_4_2 ######
###############################################

by Andreas Unterkircher

config/node-info.d/glite-ui_tar and glite-wn_tar
- remove config_vomsdir because of bug #38236 


24/06/2008 ->

###############################################
####### TAG glite-yaim-clients_R_4_0_4_2 ######
###############################################

by Gergely Debreczeni

config/functions/config_glexec:
- fixed some errors


###############################################
####### TAG glite-yaim-clients_R_4_0_4_1 ######
###############################################

by Gergely Debreczeni

config/functions/config_glexec:
- New: first version of worker node configuration on the WN

by Maria Alandes

config/node-info.d/glite-vobox:
- Update: added amga clients config in vobox

23/06/2008 ->

by Maria Alandes

config/functions/config_wn:
- Fix for bug #37001: 64bits paths for python and perl have been added to the wn.

19/06/2008 ->

by Maria Alandes

config/node-info.d/
glite-ui 
glite-ui_tar 
glite-vobox 
glite-wn 
glite-wn_tar: 
- Fix for bug #36919: config_vomsdir added in UI, WN, TARs and VOBOX.

config/functions/config_glite-ui:
- Fix for bug #37892: requested library added to UI

config/functions/config_gsissh:
- improved version 

17/06/2008 ->

by Maria Alandes

config/functions/config_glite_ui:
- Fix for bug #31211: enable service discovery for WMS and LB.

06/06/2008 ->

by Andreas Unterkircher

config/functions/config_amga_client
config/node-info.d/glite-ui, glite-ui_tar, glite-wn, glite-wn_tar 
- config for AMGA client

30/05/2008 ->

by Andreas Unterkircher

config/functions/config_ui_tar
- fix bug #22698

27/05/2008 ->

by Maria Alandes

config/defaults/glite-vobox.pre
- Add: default config variables.

config/services/glite-vobox
- New: services file for config variables.

###############################################
####### TAG glite-yaim-clients_R_4_0_3_3 ######
###############################################


11/03/2008 ->

by Maria Alandes

config/functions/config_glite_ui:
- Update: fix in UI configuration to be able to work in 3.1 WMS


###############################################
####### TAG glite-yaim-clients_R_4_0_3_2 ######
###############################################

10/03/2008 ->

by Maria Alandes

config/functions/config_glite_ui:
- Fix for bug #34346: Missing path added for WMSProxy Python API

###############################################
####### TAG glite-yaim-clients_R_4_0_3_1 ######
###############################################

15/02/2008 ->

by Maria Alandes

config/functions/config_glite_ui:
- Fix for bug #33521: SignificantAttributes has been added to glite_wms.conf.

14/12/2007 ->

by Andreas Unterkircher

config/functions/config_ui_tar:
- Fix for bug #32191

11/12/2007 ->

by Maria Alandes

config/functions/config_glite_ui:
- Fix for bug #27685: /tmp/glite/glite-ui is now created.

07/12/2007 ->

by Maria Alandes

config/functions/config_glite_ui:
- Fix for bug #32013 and #30002: Multiples LBs and WMSs can be now declared per VO

