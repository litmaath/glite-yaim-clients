Summary: glite-yaim-clients
Name: glite-yaim-clients
Version: 5.2.1
Vendor: EGI
Release: 1%dist
License: ASL 2.0
Group: System Environment/Admin Tools
Source: %{name}.src.tgz
BuildArch: noarch
Prefix: /opt/glite
Requires: glite-yaim-core
BuildRoot: %{_tmppath}/%{name}-%{version}-build
Packager: CERN

%description
This package contains the YAIM functions to configure
the UI, WN, VOBOX, TAR UI & WN, NAGIOS. 

%prep

%setup -c

%build
make install prefix=%{buildroot}%{prefix}

%files
%defattr(-,root,root)
%{prefix}/yaim/functions/config_*
%config(noreplace) %{prefix}/yaim/node-info.d/glite-*
%config(noreplace) %{prefix}/yaim/node-info.d/emi-*
%{prefix}/yaim/defaults/glite-*
%{prefix}/yaim/defaults/emi-*
%doc LICENSE
%{prefix}/yaim/etc/versions/%{name}
%{prefix}/yaim/examples/siteinfo/services/glite-*



%clean
rm -rf %{buildroot}


