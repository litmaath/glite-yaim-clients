##############################################################################
# Copyright (c) Members of the EGEE Collaboration. 2004.
# See http://www.eu-egee.org/partners/ for details on the copyright
# holders.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################
#
# NAME:        config_info_service_vobox
#
# DESCRIPTION: This function configures the dynamic service publisher
#              for VOBOX node.
#
# AUTHORS:     Maria.Alandes.Pradillo@cern.ch, Andrew.Elwell@cern.ch
#               
# NOTES:       
#
# YAIM MODULE: glite-yaim-clients
#
# CVS TAG:     $Id: config_info_service_vobox,v 1.7 2010-09-27 10:00:52 aelwell Exp $
#
##############################################################################

config_info_service_vobox_check () {
	requires $1 SITE_NAME INSTALL_ROOT VOBOX_HOST VOS
}

config_info_service_vobox_setenv () {
	yaimlog DEBUG "This function currently doesn't set any environment variables."
}

config_info_service_vobox () {

INFO_SERVICE_CONFIG=${INFO_SERVICE_CONFIG:-${INSTALL_ROOT}/glite/etc}
INFO_SERVICE_SCRIPT=${INFO_SERVICE_SCRIPT:-${INSTALL_ROOT}/glite/libexec}
INFO_PROVIDER_PATH=${INFO_PROVIDER_PATH:-${INSTALL_ROOT}/glite/etc/gip/provider}

SERVICE=VOBOX
SERVICE_HOST=${VOBOX_HOST}

SERVICE_LC=`echo ${SERVICE} | tr '[:upper:]' '[:lower:]'`

# Many of these have to be duplicated at the moment for both glue1 / glue2 versions
if [ ! -f ${INFO_SERVICE_CONFIG}/glite-info-service-${SERVICE_LC}.conf.template ]; then
  yaimlog ERROR "The template file for glite-info-service-${SERVICE_LC}.conf.template was not found in ${INFO_SERVICE_CONFIG}."
  exit 1
fi
if [ ! -f ${INFO_SERVICE_CONFIG}/glite-info-glue2-${SERVICE_LC}.conf.template ]; then
  yaimlog ERROR "The template file for glite-info-glue2-${SERVICE_LC}.conf.template was not found in ${INFO_SERVICE_CONFIG}."
  exit 1
fi

if [ ! -f ${INFO_SERVICE_SCRIPT}/glite-info-service ]; then
  yaimlog ERROR "The script file for glite-info-service was not found in ${INFO_SERVICE_SCRIPT}."
  exit 1
fi
if [ ! -f ${INFO_SERVICE_SCRIPT}/glite-info-glue2-simple ]; then
  yaimlog ERROR "The script file for glite-info-glue2-simple was not found in ${INFO_SERVICE_SCRIPT}."
  exit 1
fi

yaimlog DEBUG "Deleting a previous version of the glite-info-service-${SERVICE_LC}.conf if it exists"
rm -f ${INFO_SERVICE_CONFIG}/glite-info-service-${SERVICE_LC}.conf
yaimlog DEBUG "Deleting a previous version of the glite-info-glue2-${SERVICE_LC}.conf if it exists"
rm -f ${INFO_SERVICE_CONFIG}/glite-info-glue2-${SERVICE_LC}.conf

# Prepare get_owner list
vo_list=""
vo_acbr_list=""
for vo in ${VOS}; do
  vo_list="${vo}\\\n${vo_list}"
  vo_acbr_list="VO:${vo}\\\n${vo_acbr_list}"
done 

yaimlog DEBUG "Create the glite-info-service-${SERVICE_LC}.conf file out of the template file"
cat ${INFO_SERVICE_CONFIG}/glite-info-service-${SERVICE_LC}.conf.template | sed \
  -e "s#^get_owner =.*#get_owner = echo -e \"${vo_list}\"#" \
  -e "s#^get_acbr =.*#get_acbr = echo -e \"${vo_acbr_list}\"#" > ${INFO_SERVICE_CONFIG}/glite-info-service-${SERVICE_LC}.conf

yaimlog DEBUG "Create the glite-info-glue2-${SERVICE_LC}.conf file out of the template file"
cat ${INFO_SERVICE_CONFIG}/glite-info-glue2-${SERVICE_LC}.conf.template | sed \
  -e "s#^get_owner =.*#get_owner = echo -e \"${vo_list}\"#" \
  -e "s#^get_acbr =.*#get_acbr = echo -e \"${vo_acbr_list}\"#" > ${INFO_SERVICE_CONFIG}/glite-info-glue2-${SERVICE_LC}.conf

# Note: the configuration file may need to be altered if non standard values have been used. 

yaimlog DEBUG "Delete a previous version of the glite-info-provider-service-${SERVICE_LC}-wrapper if it exists"
rm -rf ${INFO_PROVIDER_PATH}/glite-info-provider-service-${SERVICE_LC}-wrapper

yaimlog DEBUG "Create the ${INFO_PROVIDER_PATH} in case it doesn't exist"
mkdir -p ${INFO_PROVIDER_PATH} 

yaimlog DEBUG "Create the glite-info-provider-service-${SERVICE_LC}-wrapper file"
cat << EOF > ${INFO_PROVIDER_PATH}/glite-info-provider-service-${SERVICE_LC}-wrapper
#!/bin/sh
export PATH=$PATH:${INFO_SERVICE_SCRIPT}
export ${SERVICE}_HOST=${SERVICE_HOST}
${INFO_SERVICE_SCRIPT}/glite-info-service ${INFO_SERVICE_CONFIG}/glite-info-service-${SERVICE_LC}.conf $SITE_NAME
${INFO_SERVICE_SCRIPT}/glite-info-glue2-simple ${INFO_SERVICE_CONFIG}/glite-info-glue2-${SERVICE_LC}.conf $SITE_NAME
EOF

chmod +x ${INFO_PROVIDER_PATH}/glite-info-provider-service-${SERVICE_LC}-wrapper 
 
  
return 0

}

