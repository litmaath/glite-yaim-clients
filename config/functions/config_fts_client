##############################################################################
# Copyright (c) Members of the EGEE Collaboration. 2004. 
# See http://www.eu-egee.org/partners/ for details on the copyright 
# holders.  
#
# Licensed under the Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance with the License. 
# You may obtain a copy of the License at 
#
#    http://www.apache.org/licenses/LICENSE-2.0 
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the License is distributed on an "AS IS" BASIS, 
# WITHOUT WARRANTIES OR CONDITIONS 
# OF ANY KIND, either express or implied. 
# See the License for the specific language governing permissions and 
# limitations under the License.
##############################################################################
#
# NAME :        config_fts_client 
#
# DESCRIPTION : This function configures the FTS client.
#
# AUTHORS :     Robert.Harakaly@cern.ch
#
# NOTES :      
#
# YAIM MODULE:  glite-yaim-clients
#                 
##############################################################################

function config_fts_client_check () {
  requires $1 GLITE_LOCATION
  ret=$?
}

function config_fts_client () {
 
#INSTALL_ROOT=${INSTALL_ROOT:-/opt} 

if [ -z "$FTS_SERVER_URL" ]; then
    return 0
fi

cat > ${GLITE_LOCATION}/etc/services.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>

<services>

  <service name="EGEEfts">
    <parameters>
      <endpoint>${FTS_SERVER_URL}/services/FileTransfer</endpoint>
      <type>org.glite.FileTransfer</type>
      <version>3.0.0</version>
    </parameters>
  </service>

  <service name="EGEEchannel">
    <parameters>
      <endpoint>${FTS_SERVER_URL}/services/ChannelManagement</endpoint>
      <type>org.glite.ChannelManagement</type>
      <version>3.0.0</version>
    </parameters>
  </service>

  <service name="EGEEstats">
   <parameters>
    <endpoint>${FTS_SERVER_URL}/services/FileTransferStats</endpoint>
    <type>org.glite.FileTransferStats</type>
    <version>3.0.0</version>
   </parameters>
  </service>
</services>
EOF

return 0

}

